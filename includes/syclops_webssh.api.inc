<?php
/**
 * @file
 * Complete WebSSH API.
 */
/**
 * Menu Page Callback for 'Sync WebSSH Terminal Session'.
 */
function syclops_webssh_session_sync($grant) {
	$data = array();
	// Checking for raw post data.
	if(isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
		// Get raw post data.
		$post_data = $GLOBALS['HTTP_RAW_POST_DATA'];	
		$post_data = json_decode($post_data);
		// Extracting og_group_ref value.
      $og = field_get_items('node',$grant,'og_group_ref');
      $og = reset($og);
		// Loading node entity.
		$og = node_load($og['target_id']);
   	// Extracting field_grant_ssh_config value.
   	$ssh_config = field_get_items('node',$grant,'field_grant_ssh_config');
   	$ssh_config = reset($ssh_config);
		// Load node entity.
		$ssh_config = node_load($ssh_config['nid']);
      // Extracting field_grant_user value.
      $user = field_get_items('node',$grant,'field_grant_user');
      $user = reset($user);
		// Load user entity.
		$user = user_load($user['uid']);	
		// Instantiating stdClass object.
		$node = new stdClass();
		$node->type = 'session';
		// Preparing node object.
		node_object_prepare($node);
		// Setting title.
		$node->title = $user->name.'-'.$ssh_config->title.'-'.time();
		// Setting values to fields.
		$node->uid = $og->uid;	
		$node->language = LANGUAGE_NONE;		
		$node->og_group_ref[$node->language][0]['target_id'] = $og->nid;
		$node->field_session_grant[$node->language][0]['nid'] = $grant->nid;	
		$node->field_session_id[$node->language][0]['value'] = $post_data->session;	
		$node->field_session_status[$node->language][0]['value'] = 'activated';
		$node->field_session_playback_status[$node->language][0]['value'] = 0; 
		// Saving node to data base.
		$node = node_submit($node);
		// Saving node.
		node_save($node);
		// Loading .inc files.
   	module_load_include('inc','syclops_log','includes/syclops_log.api');
   	// Array of objects.
   	$objects = array();
   	// Array of values.
   	$params = array();
		// Building data.
		$params['uid'] = $user->uid;
		$objects['field_msg_project_ref'] = $og;
		$objects['field_msg_ssh_config_ref'] = $ssh_config;
		$objects['field_msg_grant_ref'] = $grant;
		$objects['field_msg_session_ref'] = $node;
		// Logging message.
		syclops_log_message('msg_create_session',$params,$objects);
		// Inserting values to db.				
		db_insert('syclops_webssh_sessions')
			->fields(
				array(
					'user_id' => (int)$user->uid,
					'grant_id' => (int)$grant->nid,
					'session_id' => (int)$node->nid
				)
       	)
			->execute();
		// Setting auto nodetitle programmatically.
		auto_nodetitle_set_title($node);                 
		// Saving new revision.
		_node_save_revision($node,$node->uid,'vid');
		// Resaving the node.
		node_save($node);
		$data['id'] = 	$node->nid;
		$data['status'] = 'success';
	}else{
		$data['status'] = 'failure';
	}
	// Sending JSON data.
	drupal_json_output($data);	
}
/**
 * Menu Page Callback for 'Get Identity'.
 */
function syclops_webssh_identity_get($grant) {
	$data = array();
   // Extracting field_grant_ssh_config value.
   $ssh_config = field_get_items('node',$grant,'field_grant_ssh_config');
   $ssh_config = reset($ssh_config);
   // Load node entity.
   $ssh_config = node_load($ssh_config['nid']);
   // Extracting field_ssh_config_private_key value.
	$private_key = field_get_items('node',$ssh_config,'field_ssh_config_private_key');
	$private_key = reset($private_key);
	$data['private_key']	= $private_key['value'];
	// Sending JSON data.
	drupal_json_output($data);
}
/**
 * Menu Page Callback for 'Get Session Playback'.
 */
function syclops_webssh_session_playback($grant) {
	 global $base_url;
   // Checking for raw post data.
   if(isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
      // Get raw post data.
      $post_data = $GLOBALS['HTTP_RAW_POST_DATA'];			
      $post_data = json_decode($post_data);
		// Checking post data status.
		if($post_data->status=='success') {
			//set timestamp to UTC
			date_default_timezone_set("UTC");	
			// Load node entity.
			$session = node_load($post_data->id);
   		// Extracting field_session_id value.
   		$session_id = field_get_items('node',$session,'field_session_id');
   		$session_id = reset($session_id);
			// Preparing object for edit.
			node_object_prepare($session);
			// Filename of the playback.
			$pattern = '/https?:\/\//i';
     	$replace = '';
      $file_name = preg_replace($pattern, $replace, $base_url);
			$file_name .= '_'.$post_data->id.'.html';

			// Playback file storage path.
			$directory = 'public://playbacks';
			// Constructing drupal uri.
			$uri = $directory.'/'.$file_name;
			// Playback and Download button markup.
			$markup = '<iframe id="session-playback" src='.file_create_url($uri).' width=100% frameborder=0 scrolling=no allowTransparency=true></iframe>';
			// Setting field values.
			$session->field_session_playback[$session->language][0]['value'] = file_create_url($uri);
			$session->field_session_render_playback[$session->language][0] = array(
				'value' => $markup,
				'format' => 'full_html'
			);
			//$session->field_session_end_time[$session->language][0]['value'] = gmdate("Y-m-d H:i:s");//date("Y-m-d H:i:s", time());
			//Saving node
			node_save($session);
		}
		if($post_data->status=='failure') {
			// Loading node entity.
         $session = node_load($post_data->id);
			// Preparing object for edit.
         node_object_prepare($session);
			// Setting field values.
         $session->field_session_log[$session->language][0]['value'] = 'An unexpected error occurred';
			// Saving node.
         node_save($session);  						
		}
	}       
}
/**
 * Menu Page Callback for 'Wait WebSSH Terminal Session'.
 */
function syclops_webssh_session_wait($grant) {	
   $data = array();
   // Checking for raw post data.
   if(isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
      // Get raw post data.
      $post_data = $GLOBALS['HTTP_RAW_POST_DATA'];
      $post_data = json_decode($post_data);
		// Forcing to sleep.
		sleep($post_data->wait);
      $data['status'] = 'success';
   }else{
		$data['status'] = 'failure';
	}
   // Sending JSON data.
   drupal_json_output($data);
}

function syclops_webssh_session_notify($grant) {
  // Checking for raw post data.
   if(isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
      // Get raw post data.
      $post_data = $GLOBALS['HTTP_RAW_POST_DATA'];
      $post_data = json_decode($post_data);
			$session = node_load($post_data->id);
		// for debugging purpose
		//watchdog('debug', '<pre>'. print_r($post_data, TRUE) .'</pre>'); 
    // Checking post data status.
		if($post_data->status == 'success') {
      // Loading node entity.
      // Preparing object for edit.
         node_object_prepare($session);
      // Setting field values.
         $session->field_session_playback_status[$session->language][0]['value'] = 1;
	 $session->field_session_end_time[$session->language][0]['value'] = $post_data->duration;
      // Saving node.
         node_save($session);
    } 

	 	if($post_data->status == 'failure') {
      // Preparing object for edit.
         node_object_prepare($session);
      // Setting field values.
         $session->field_session_playback_status[$session->language][0]['value'] = 2;
      // Saving node.
         node_save($session);
		}

	}
}
