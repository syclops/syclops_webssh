<?php
/**
 * @file
 * Menu related functions.
 */
/**
 * Menu Page Callback for 'Connect WebSSH Terminal'.
 */
function syclops_webssh_connect($grant) {
   // Extracting field_grant_user value.
	$grant_user = field_get_items('node',$grant,'field_grant_user');
   $grant_user = reset($grant_user);
	// Loading user entity.
	$user = user_load($grant_user['uid']);
   // Extracting og_group_ref value.
   $og = field_get_items('node',$grant,'og_group_ref');
   $og = reset($og);
	// Loading node entity.
	$og = node_load($og['target_id']);
	// Getting webssh_host value.
	$webssh_host = variable_get('webssh_host');
   // Getting webssh_port value.
   $webssh_port = variable_get('webssh_port');
   // Getting webssh_api_key value.
   $webssh_api_key = variable_get('webssh_api_key');
	// Getting webssh_secret_key value.
   $webssh_secret_key = variable_get('webssh_secret_key');
	// Extracting field_grant_ssh_config value.
   $ssh_config = field_get_items('node',$grant,'field_grant_ssh_config');
   $ssh_config = reset($ssh_config);
	// Load node entity.
	$ssh_config = node_load($ssh_config['nid']);
	// Extracting field_ssh_config_machine_user value.
	$machine_user = field_get_items('node',$ssh_config,'field_ssh_config_machine_user');
	$machine_user = reset($machine_user);
	// Extracting field_ssh_config_machine value.
   $machine = field_get_items('node',$ssh_config,'field_ssh_config_machine');
   $machine = reset($machine);	
	// Params to be passed to template.
	$params = array();	
  	$params['user_name'] = $user->name;
	$params['webssh_host'] = $webssh_host;
	$params['webssh_port'] = $webssh_port;
	$params['webssh_api_key'] = $webssh_api_key;
	$params['webssh_secret_key'] = $webssh_secret_key;
	$params['machine_user'] = $machine_user['value'];
	$params['machine'] = $machine['value'];
	$params['grant_nid'] = $grant->nid;
	$params['project_nid'] = $og->nid;
	// Getting custom theme hook.
   $html = theme('webssh_terminal_connect',$params);
	// Sending drupal headers.
   drupal_send_headers();
	// Printing html.
   print $html;	
}
