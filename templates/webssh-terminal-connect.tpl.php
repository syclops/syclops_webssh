<?php
/**
 * @file
 * Rendered on 'Connect WebSSH Terminal' Menu Page Callback.
 */
?>
<!DOCTYPE html>
<html>
<head>
	<?php
		// Getting base url.
		global $base_url;
		// Setting webssh url.
   	$webssh_base_url = 'https://'.$webssh_host.':'.$webssh_port;
	?>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
	<meta name="viewport" content="target-densitydpi=device-dpi,width=device-width,initial-scale=1.0,minimum-scale=1.0,user-scalable=0">
  <link rel="stylesheet" href="<?php print $webssh_base_url ?>/static/gateone.css" type="text/css" media="screen"/>	
	<!-- HTML5 element support for IE6-8 -->
  	<!--[if lt IE 9]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  	<![endif]-->
   <style type="text/css" media="screen">
     html,body { background : transparent; }
     #gateone .✈terminal {
			padding : 7px 10px 0px 4px;
			margin-right : -60px;
			width : 100%;
     }
		 span.✈screen {
      margin-top : 100px;
     }

	</style>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>	
	<div id="gateone" ></div>
	<?php			
		//Base name
		$pattern = '/https?:\/\//i';
    $replace = '';
    $base_name = preg_replace($pattern, $replace, $base_url);
		// Building auth object.
   	$webssh_auth = array(
			'api_key' => $webssh_api_key,
			'upn' => $user_name,
			'timestamp' => time().'000',
			'signature_method' => 'HMAC-SHA1',
			'api_version' => '1.0'
		);	
		// Generating signature.
   	$webssh_auth['signature'] = hash_hmac('sha1',$webssh_auth['api_key'].$webssh_auth['upn'].$webssh_auth['timestamp'],$webssh_secret_key);
	?>
	<script>
			var SyclopsWorker = {};
      SyclopsWorker.url = '<?php print $base_url.'/sites/all/themes/syclops/js/term_ww.js' ?>';
      var url = '<?php print $webssh_base_url ?>';
      var scriptTag = document.createElement('script');
		 // Loading main javascript file.
      scriptTag.src = url + '/static/gateone.js';
			scriptTag.async = true;
      scriptTag.onload = function() {
			// Initializing GateOne with complete settings.
			GateOne.init({
				url:url,
				embedded:false,
				disableTermTransitions:true,
				showTitle:false,
				showToolbar:false,
				fillContainer:true,
				colors: 'default',//'gnome-terminal',
				webWorker: '<?php print $base_url.'/sites/all/themes/syclops/js/term_ww.js' ?>',
				//playbackFrames:10000,
				auth:<?php print json_encode($webssh_auth) ?>,		
				// Syclops settings object.
				syclopsSettings:{
					// Session settings object.
					session:{
						id:null,
						baseName : '<?php print($base_name) ?>',
						syncURL:'<?php print $base_url.'/webssh/'.$grant_nid.'/session/sync' ?>',
						playbackURL:'<?php print $base_url.'/webssh/'.$grant_nid.'/session/playback' ?>',
						waitURL:'<?php print $base_url.'/webssh/'.$grant_nid.'/session/wait' ?>',
						notifyURL:'<?php print $base_url.'/webssh/'.$grant_nid.'/session/notify' ?>',
						exitURL:null						
					},
					// Identity settings object.
					identity:{
						fileName:'id_rsa',
						getURL:'<?php print $base_url.'/webssh/'.$grant_nid.'/identity/get' ?>'
					},
					// SSH settings object.
					ssh:{
						autoConnectURL:'ssh://<?php print $machine_user ?>@<?php print $machine ?>:22'
					},
					// Playback settings object.
					playback:{
						storagePath:'<?php print drupal_realpath('public://playbacks') ?>'
					},
				}
			});
     	}
		document.body.appendChild(scriptTag); 
   </script>
</body>
</html>
